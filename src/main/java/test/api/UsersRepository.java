package test.api;

import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class UsersRepository {

    private Map<String, User> users = new HashMap<>();

    @PostConstruct
    public void init() {
        users.put("1", new User("1", "Jan", "Kowalski", "jan.kowalski@gmail.com"));
    }

    public Optional<User> getById(String userId) {
        return Optional.ofNullable(users.get(userId));
    }

    public void save(User user) {
        users.put(user.getId(), user);
    }
}
