package test.api

import spock.lang.Specification
import spock.lang.Unroll

class SpockFeaturesTest extends Specification {

    @Unroll
    def "data driven test #a + #b == #exp"() {
        expect:
        a + b == exp

        where:
        a | b || exp
        1 | 2 || 3
        4 | 4 || 8
    }

    def "field extraction"() {
        given:
        List<User> users = [userWithId("1"), userWithId("2")]

        expect:
        users.id == ["1", "2"]
    }

    def userWithId(String id) {
        new User(id, "firstName", "lastName", "email")
    }
}
