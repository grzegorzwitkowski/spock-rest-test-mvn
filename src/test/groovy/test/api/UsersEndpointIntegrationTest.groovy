package test.api

import groovyx.net.http.RESTClient
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@ContextConfiguration
@SpringBootTest(webEnvironment = RANDOM_PORT)
class UsersEndpointIntegrationTest extends Specification {

    @LocalServerPort
    int port

    RESTClient restClient

    void setup() {
        restClient = new RESTClient("http://localhost:$port")
        restClient.handler.failure = restClient.handler.success
    }

    def "should get user data"() {
        given:
        restClient.post([
                path              : "/users",
                body              : """{ "id": "2", "firstName": "Anna", "lastName": "Nowak", "email": "anna.nowak@email.com" }""",
                requestContentType: "application/json"
        ])

        when:
        def response = restClient.get([path: "/users/2"])

        then:
        with(response.responseData) {
            firstName == "Anna"
            lastName == "Nowak"
            email == "anna.nowak@email.com"
        }
    }

    def "should return 404 when user was not found"() {
        when:
        def response = restClient.get([path: "/users/3"])

        then:
        response.status == 404
    }
}
